#!/usr/bin/env python

import sys, pygame, init, display
from pygame.locals import *

global windows

pos = [0, 0]

# This function will do an infinite loop, and wait for a event
# if he have no event, this loop will just refresh the screen

continuer = True
windows = init.init_pygame()
i = 0
shoot = False
pygame.key.set_repeat(1, 1)
while continuer == True:
    key = pygame.key.get_pressed()
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if key[K_a]:
                shoot = True
            if key[K_LEFT] and pos[0] > -23:
                pos[0] -= 2
            if key[K_RIGHT] and pos[0] < 427:
                pos[0] += 2
            if key[K_UP] and pos[1] > -23:
                pos[1] -= 2
            if key[K_DOWN] and pos[1] < 427:
                pos[1] += 2
        elif event.type == QUIT:
            continuer = False
    display.display_windows(windows, i, pos, shoot)
    shoot = False
    if i != 6:
        i += 1
    else:
        i = 0
