#!/usr/bin/env python

import sys, pygame, init, weapon
from pygame.locals import *

# load all frame

global character
character = (pygame.image.load("character/character_first_frame.png"),
             pygame.image.load("character/character_second_frame.png"),
             pygame.image.load("character/character_third_frame.png"),
             pygame.image.load("character/character_fourth_frame.png"),
             pygame.image.load("character/character_fithe_frame.png"),
             pygame.image.load("character/character_sixth_frame.png"),
             pygame.image.load("character/character_seventh_frame.png"))

background = pygame.image.load("character/texture.jpg")

# display the background

def display_background(background, pos_bg, windows):
     j = pos_bg
     while j < 450:
          i = 0
          while i != 450:
               windows.blit(background, (i, j))
               i += 150
          j += 150

# No static variable in python, so I must simulate one with global
# So I move the first position of the map

def static_num():
     global num
     num = num + 2
     if num > 150:
          num = 0
     return num
num = 0

# this function will call the function for display pictures
# and refresh the image

def display_windows(windows, i, pos, shoot):
     display_background(background, static_num() - 150, windows)
     if shoot == True:
          weapon.use_shoot(windows, pos)
     windows.blit(character[i], pos)
     pygame.display.flip()
