#!/usr/bin/env python

import pygame
from pygame.locals import *

# this function will just draw a line

def use_shoot(windows, pos):
    pygame.draw.line(windows, (255, 0, 0), 
                     (pos[0] + 25, pos[1]), (pos[0] + 25, 0))
