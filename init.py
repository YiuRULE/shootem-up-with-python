#!/usr/bin/env python

import sys, pygame
from pygame.locals import *

# this function will init, like display the windows or other stuff

def init_pygame():
    resolution = 450, 450
    pygame.init()
    pygame.display.init()
    if pygame.display.get_init() is False:
        print "cannot init pygame"
        exit
    windows = pygame.display.set_mode(resolution)
    return windows;

# this function will quit pygame properly

def quit_game():
    pygame.display.quit()
    sys.exit()
